// native modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule} from '@angular/common/http';


// components
import { AppComponent } from './app.component';
import { AppNavComponent } from './app-nav/app-nav.component';
import { CatsComponent } from './cats/cats.component';
import { GridComponent } from './grid/grid.component';
import { AddItemComponent, DialogAdd } from './add-item/add-item.component';
import { TableComponent } from './table/table.component';
import { EditButtonComponent, DialogEdit } from './edit-button/edit-button.component';
import { DeleteButtonComponent, DialogDelete } from './delete-button/delete-button.component';

// material modules
import {DragDropModule} from '@angular/cdk/drag-drop';
import {ScrollingModule} from '@angular/cdk/scrolling';
import {CdkTableModule} from '@angular/cdk/table';
import {CdkTreeModule} from '@angular/cdk/tree';
import {
  MatAutocompleteModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule, MatFormField, MatFormFieldModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatTreeModule,
} from '@angular/material';
import {DataService} from "./data.service";




const appRoutes: Routes = [
  { path: '', component: CatsComponent },
  { path: 'grid', component: GridComponent }];

@NgModule({
  entryComponents: [
    AppNavComponent,
    AddItemComponent,
    DialogAdd,
    DialogEdit,
    DialogDelete,
    TableComponent
  ],
  declarations: [
    AppComponent,
    AppNavComponent,
    CatsComponent,
    GridComponent,
    AddItemComponent,
    DialogAdd,
    DialogEdit,
    DialogDelete,
    TableComponent,
    EditButtonComponent,
    DeleteButtonComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatNativeDateModule,
    MatIconModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatCardModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatNativeDateModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    RouterModule.forRoot(appRoutes,
      { enableTracing: true })
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
