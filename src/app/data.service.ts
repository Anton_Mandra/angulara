import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable} from 'rxjs';

import {TableData} from './data';

@Injectable()
export class DataService {
   url  = 'http://127.0.0.1:8000/api/data';


constructor(private http: HttpClient) {}

getData(): Observable<TableData[]> {
return this.http.get<TableData[]>(this.url);
}

deleteItem(id) {
return this.http.delete(this.url + '/' + id);
}
}

