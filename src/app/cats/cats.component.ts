import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cats',
  templateUrl: './cats.component.html',
  styleUrls: ['./cats.component.css']
})
export class CatsComponent implements OnInit {

  cats = ['https://i.ytimg.com/vi/mK72EwuxZAU/hqdefault.jpg',
    'http://marmazov.ru/wp-content/uploads/2017/05/kotiki.jpg',
    'http://www.catgallery.ru/kototeka/wp-content/uploads/2015/07/Kotiki-pyut-vodu-2.jpg',
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS38tM14g9vApkfZRGJzF_7dUcsFcDjETfQhrxQOTqlF27gwEpM',
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ_EhjWN-QuIFum-C4HyyLmlOQoRDcq5kIu6NoxCqhZD5WPQrhN'];

  constructor() { }

  ngOnInit() {
  }

}
