import {Component, Inject} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-add-item',
  templateUrl: './add-item.component.html',
  styleUrls: ['./add-item.component.css']
})
export class AddItemComponent {
data = {
  color: 'string',
  name: 'string'};

  constructor(public dialog: MatDialog) {}

  openDialog(): void {
    const dialogRef = this.dialog.open(DialogAdd);

  dialogRef.afterClosed().subscribe(result => {
    console.log('The dialog was closed');

  });
}

}

@Component({
  selector: 'dialog-add',
  templateUrl: 'dialog-add.html',
})
export class DialogAdd {

  constructor(
    public dialogRef: MatDialogRef<DialogAdd>) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

}
