import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {Response} from '@angular/http';

// import { Observable} from 'rxjs';
// import { map } from 'rxjs/operators';

import {DataService} from '../data.service';
import {TableData} from '../data';


@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css'],
  providers: [DataService]
})
export class TableComponent implements OnInit {
  displayedColumns: string[] = ['id', 'name', 'color', 'action'];
  dataSource: MatTableDataSource<TableData>;
  tableData = [];


  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  constructor(private dataService: DataService) {

   this.dataService.getData()
      .subscribe(res => {
        for (const row of res) {
          this.tableData.push(row);
        }
        this.dataSource = new MatTableDataSource(this.tableData);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });

    // Assign the data to the data source for the table to render


  }


  ngOnInit() {
    //

    console.log(this.paginator);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}

